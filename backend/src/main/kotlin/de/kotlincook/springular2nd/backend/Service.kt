package de.kotlincook.springular2nd.backend

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin(origins = ["http://localhost"])
@RestController
class Service {

    data class Title(var text: String)

    @GetMapping("/sayhello", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun sayHello(): Title {
        return Title("Hallo Springular2nd")
    }
}