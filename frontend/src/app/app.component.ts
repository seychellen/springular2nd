import {Component} from '@angular/core';
import {ClientService} from "./client.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Titel';

  constructor(private clientService: ClientService) {
    clientService.getHelloMess().subscribe(
    resp => this.title = resp.text);
  }
}
