import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Title } from "./Title"

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) {
  }

  getHelloMess(): Observable<Title> {
    return this.http.get<Title>("/sayhello");
  }

}
